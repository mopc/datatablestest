﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace DataTablesTest.Pages
{
    public class IndexModel : PageModel
    {
        [BindProperty]
        public int RowCount { get; set; } = 100;

        public void OnGet()
        {

        }

        public IActionResult OnPostAsync()
        {
            return Page();
        }
    }
}
