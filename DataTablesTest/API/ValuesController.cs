﻿using System.Collections.Generic;
using DataTablesTest.Core;
using DataTablesTest.Models;
using Microsoft.AspNetCore.Mvc;

namespace DataTablesTest.API
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        [Route("getitems/{count}")]
        public IEnumerable<TableItem> GetItems(int count)
        {
            for (var i = 0; i < count; i++)
            {
                var item = new TableItem
                {
                    Item1 = RandomWordGenerator.Generate(),
                    Item2 = RandomWordGenerator.Generate(),
                    Item3 = RandomWordGenerator.Generate(),
                    Item4 = RandomWordGenerator.Generate(),
                    Item5 = RandomWordGenerator.Generate(),
                    Item6 = RandomWordGenerator.Generate(),
                    Item7 = RandomWordGenerator.Generate(),
                    Item8 = RandomWordGenerator.Generate(),
                };
                yield return item;
            }
        }

        [Route("getitemsarray/{count}")]
        public IEnumerable<IEnumerable<string>> GetItemsArray(int count)
        {
            for (var i = 0; i < count; i++)
            {
                yield return new[]
                {
                    RandomWordGenerator.Generate(),
                    RandomWordGenerator.Generate(),
                    RandomWordGenerator.Generate(),
                    RandomWordGenerator.Generate(),
                    RandomWordGenerator.Generate(),
                    RandomWordGenerator.Generate(),
                    RandomWordGenerator.Generate(),
                    RandomWordGenerator.Generate()
                };
            }
        }
    }
}
