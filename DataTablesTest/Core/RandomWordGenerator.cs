﻿using System;
using System.Linq;

namespace DataTablesTest.Core
{
    public static class RandomWordGenerator
    {
        public static string Generate()
        {
            var baseString = Guid.NewGuid().ToString();
            var alpha = string.Join("", baseString.Where(char.IsLetter));
            return alpha;
        }
    }
}
